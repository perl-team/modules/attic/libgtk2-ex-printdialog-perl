libgtk2-ex-printdialog-perl (0.03-4) unstable; urgency=medium

  * Team upload

  [ Ansgar Burchardt ]
  * Update my email address.
  * debian/control: Convert Vcs-* fields to Git.

  [ Salvatore Bonaccorso ]
  * debian/copyright: Replace DEP5 Format-Specification URL from
    svn.debian.org to anonscm.debian.org URL.
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * Remove Jeffrey Ratcliffe from Uploaders. Thanks for your work!

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ Florian Schlichting ]
  * Update d/copyright to copyright-format 1.0
  * Update license paragraphs to commonly used versions
  * Bump dh compat to level 11
  * Declare compliance with Debian Policy 4.1.4
  * Autopkgtest: skip Win32 module, we lack required deps

 -- Florian Schlichting <fsfs@debian.org>  Sat, 30 Jun 2018 17:35:02 +0200

libgtk2-ex-printdialog-perl (0.03-3) unstable; urgency=low

  [ Damyan Ivanov ]
  * debian/control: Changed: Maintainer set to Debian Perl Group <pkg-
    perl-maintainers@lists.alioth.debian.org> (was: Jeffrey Ratcliffe
    <Jeffrey.Ratcliffe@gmail.com>); Jeffrey Ratcliffe
    <Jeffrey.Ratcliffe@gmail.com> moved to Uploaders.
  * debian/watch: use dist-based URL.

  [ gregor herrmann ]
  * debian/control: Added: ${misc:Depends} to Depends: field.

  [ Nathan Handler ]
  * debian/watch: Update to ignore development releases.

  [ Ansgar Burchardt ]
  * Use source format 3.0 (quilt).
  * Refresh debian/rules for debhelper 7.
  * debian/control: Use module name instead of package name in description.
  * debian/control: Remove XS-DM-Upload-Allowed: yes.
  * Bump Standards-Version to 3.8.4.
  * Add myself to Uploaders.

  [ gregor herrmann ]
  * Remove version from (build) dependency.

 -- Ansgar Burchardt <ansgar@43-1.org>  Sun, 14 Feb 2010 17:36:58 +0900

libgtk2-ex-printdialog-perl (0.03-2) unstable; urgency=low

  * + liblocale-gettext-perl build dependency
    Closes: #452369 (FTBFS: missing build-dependency on liblocale-gettext-perl)
  * + DM-Upload-Allowed: yes

 -- Jeffrey Ratcliffe <Jeffrey.Ratcliffe@gmail.com>  Wed, 27 Feb 2008 20:08:15 +0100

libgtk2-ex-printdialog-perl (0.03-1) unstable; urgency=low

  * Initial Release. (Closes: #452369)
  * Patch to ensure Net::CUPS::Destination is correctly called,
    and that the printer names are passed to the ComboBox.

 -- Jeffrey Ratcliffe <Jeffrey.Ratcliffe@gmail.com>  Sun, 18 Nov 2007 18:38:04 +0100
